import { createColumnHelper } from '@tanstack/react-table'
import { Customer } from './user.types'
import ImageText from '@/app/components/core/imageText/imageText'
import Status from '@/app/components/core/status/status'
import Actions from '@/app/components/core/actions/actions'
const columnHelper = createColumnHelper<Customer>()

const customerColumns = [
	columnHelper.accessor(row => row.id, {
		id: 'id',
		cell: info => <span>{info.getValue()}</span>,
		header: () => <span>Id</span>
	}),
	columnHelper.accessor(row => row.company, {
		id: 'company',
		cell: info => <ImageText>{info.getValue()}</ImageText>,
		header: () => <span>company</span>
	}),
	columnHelper.accessor(row => row.code, {
		id: 'code',
		cell: info => <span>{info.getValue()}</span>,
		header: () => <span>Code</span>
	}),
	columnHelper.accessor(row => row.first_name, {
		id: 'first_name',
		cell: info => <span>{info.getValue()}</span>,
		header: () => <span>First Name</span>
	}),
	columnHelper.accessor(row => row.last_name, {
		id: 'last_name',
		cell: info => <span>{info.getValue()}</span>,
		header: () => <span>Last Name</span>
	}),
	columnHelper.accessor(row => row.access, {
		id: 'access',
		cell: info => <span>{info.getValue()}</span>,
		header: () => <span>Access</span>
	}),
	columnHelper.accessor(row => row.status, {
		id: 'status',
		cell: info => <Status status={info.getValue()}>{!info.getValue() ? 'Canceled' : 'Delivered'}</Status>,
		header: () => <span>Status</span>
	}),
	columnHelper.accessor(row => row.action, {
		id: 'action',
		enableSorting: false,
		cell: info => <Actions></Actions>,
		header: () => <span>Actions</span>
	})
]

export default customerColumns