import { ReactNode } from "react"
// access: 1
// code: 104
// company: "McGlynn Inc"
// first_name: "Aile"
// id: 1
// last_name: "Gatlin"
// status: false

export type Customer = {
	access: number,
	code: number,
	company: string,
	first_name: string,
	id: number,
	last_name: string,
	status: any,
	action: ReactNode
}
