import customerColumns from "./userColumns"
import Table from '@/app/components/table/table'
import Loading from '@/app/components/core/loading/loading'
import { useQuery } from "@tanstack/react-query";
import axios from "axios"

const UserTable = ({ columnFilters }: any) => {
	const { isLoading, error, data } = useQuery({
		queryKey: ['UserTable'],
		queryFn: async () =>
			await axios.get('https://mocki.io/v1/3361b3fd-79ad-45c2-aba4-3ee66cc9230c').then((response) => response.data)
	  })

	if (isLoading) return <Loading />

	if (error) return 'An error has occurred: ' + error


	return (
		<>
			<Table
				data={data}
				columns={customerColumns} 
				columnFilters={columnFilters}
			/>
		</>
	)
}

export default UserTable