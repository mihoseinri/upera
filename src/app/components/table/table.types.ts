// router - different tables
// useSuspenseQuery

import { ColumnDef } from '@tanstack/react-table'

export type TableProps<T extends Record<string, unknown>> = {
	data: T[],
	columns: ColumnDef<T, any>[],
	columnFilters: any
}

