'use client'
import React, { useEffect, useState } from "react";
import { TableProps } from './table.types'
import Image from "next/image"
import { 
	RowModel, 
	useReactTable, 
	getCoreRowModel, 
	flexRender, 
	ColumnDef, 
	getFilteredRowModel, 
	getSortedRowModel, 
	getPaginationRowModel
} from '@tanstack/react-table'

const Table = <T extends Record<string, any>>({ data, columns, columnFilters }: TableProps<T>) => {
	const table = useReactTable({
		columns, 
		data,
		state: {
			columnFilters
		},
		getCoreRowModel: getCoreRowModel(),
		getFilteredRowModel: getFilteredRowModel(),
		getPaginationRowModel: getPaginationRowModel(),
		getSortedRowModel: getSortedRowModel()
	});

	const numbers = Array.from({ length: table.getPageCount() }, (_, index) => index);
	return (
		<div>
			<div className="flex justify-center">
				<table className="my-auto">
					<thead>
						{table.getHeaderGroups().map(headerGroup => (
							<tr
								key={headerGroup.id}
								className="text-gray-800">
								{headerGroup.headers.map(header => (
									<th
										key={header.id}
										className="px-4 pr-2 py-4 text-left">
										<div className="flex items-center gap-2">
											<span>
												{header.isPlaceholder
													? null
													: flexRender(
														header.column.columnDef.header,
														header.getContext()
												)}
											</span>
											<span className="cursor-pointer">
												{
													header.column.getCanSort() && <Image src="./assets/sort.svg" alt="me" onClick={header.column.getToggleSortingHandler()} width="16" height="16" />
												}
											</span>
										</div>
									</th>
								))}
							</tr>
						))}
					</thead>
					<tbody>
						{table.getRowModel().rows.map(row => (
							<tr key={row.id}>
								{row.getVisibleCells().map(cell => (
									<td key={cell.id}>
										{flexRender(cell.column.columnDef.cell, cell.getContext())}
									</td>
								))}
							</tr>
						))}
					</tbody>
				</table>
			<div />
		</div>
		<div className={numbers.length === 1 ? 'hidden' : 'pagination p-4 w-full flex justify-center items-center gap-3'}>
			<div className={!table.getCanPreviousPage() ? 'hidden': 'prev cursor-pointer'} onClick={() => table.previousPage()}>Prev</div>
				<div className="numbers flex items-center gap-3">
					{
						numbers.map(number => {
							return (
								<div className={number === table.getState().pagination.pageIndex ? 'item active cursor-pointer' : 'item cursor-pointer'} onClick={() => table.setPageIndex(number)}>{ number + 1 }</div>
							)
						})			
					}
				</div>
				<div className={!table.getCanNextPage()? 'hidden': 'next cursor-pointer'} onClick={() => table.nextPage()}>Next</div>
			</div>
		</div>
	)
}


export default Table