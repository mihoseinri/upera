import React from "react";
import { statusProps } from "./status.type";


const Status = ({ children, status }: statusProps) => {
	let className = 'status' + ' ' + status
	return (
		<div className={className}>
			{ children }
		</div>
	)
}

export default Status