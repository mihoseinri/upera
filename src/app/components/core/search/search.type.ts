export type ColumnFilter = {
  id: string;
  value: number;
}