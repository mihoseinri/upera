import Image from 'next/image'
import { ColumnFilter } from './search.type'

const Search = ({ columnFilters, setColumnFilters }: any) => {
    const searchWithHeader = columnFilters.find((item: { id: string }) => item.id)?.value || ''
    
    const onFilterChange = (id: any, value: any): void => {
        setColumnFilters((prev: ColumnFilter[]) => {
          const updatedFilters = prev.filter((filter: ColumnFilter) => filter.id !== id)

          const newFilter: ColumnFilter = { id, value }
          return [...updatedFilters, newFilter]
        });
      };
    
    return (
        <div className="ml-12 relative">
            <div className='searchBox flex items-center gap-2'>
                <div>
                    <Image src="./assets/search.svg" alt="me" width="16" height="16" />
                </div>
                <input type="text" onChange={(e) => onFilterChange('first_name', e.target.value)} value={searchWithHeader} placeholder="Search..." />
            </div>
        </div>
    )
}

export default Search