const Loading = () => {
    return (
        <div className="simple-spinner">
            <span></span>
        </div>
    )
}

export default Loading