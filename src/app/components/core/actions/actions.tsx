import React from "react"
import Image from "next/image"

const Actions = () => {
	return (
		<div className="flex items-center gap-4">
			<Image src='/assets/edit.svg' alt="edit" width="24" height="24"></Image>
			<Image src='/assets/trash.svg' alt="edit" width="24" height="24"></Image>
		</div>
	)
}

export default Actions