import { ReactNode } from "react"

export type ImageTextProps = {
	children: ReactNode
}