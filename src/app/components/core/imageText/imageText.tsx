import React from "react"
import { ImageTextProps } from "./imageText.type"

const ImageText: React.FC<ImageTextProps> = ({ children }) => {
	return (
		<div className='flex items-center gap-2'><div><img src="https://picsum.photos/seed/picsum/32/32" className='rounded-lg' alt="" /> </div> <div>{ children }</div></div>
	)
}

export default ImageText