import Image from 'next/image'
import React, { use, useContext } from "react";
import Search from './core/search/search';
const Header = ({ columnFilters, setColumnFilters }: any) => {
	return (
		<div className="p-4">
			<div className="flex items-center  justify-between">
				<div className='flex'>
					<div className="flex items-center gap-6">
						<span>Show</span>
						<div>
							<select name="" className="entries" id="">
								<option value="10">10</option>
								<option value="20">20</option>
								<option value="30">30</option>
							</select>
						</div>
						<span>entries</span>
					</div>
					<Search columnFilters={columnFilters} setColumnFilters={setColumnFilters} />
				</div>
				<div>
					<button className='addCustomer px-3 py-2 text-white'>
						<div className='flex items-center gap-2'>
							<Image src="./assets/plus.svg" alt="me" width="16" height="16" />
							<span>Add Customer</span>
						</div>
					</button>
				</div>
			</div>
		</div>
	)
}

export default Header