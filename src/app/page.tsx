"use client"
import Header from '@/app/components/header'
import UserTable from '@/users/userTable'
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { useState } from 'react'
const queryClient = new QueryClient()

export default function Home() {
	const [columnFilters, setColumnFilters] = useState([])
  return (
    <QueryClientProvider client={queryClient}>
      <div>
          <Header columnFilters={columnFilters} setColumnFilters={setColumnFilters}></Header>
          <UserTable columnFilters={columnFilters} />
      </div>
    </QueryClientProvider>
  )
}
